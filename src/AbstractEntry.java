import java.util.Date;

public abstract class AbstractEntry {
    public String author;
    public Date date;
    public String message;
    public enum Tyoe {
        ENTRY, REPOST, COMMENT;
    }

    public void showContent(){
        System.out.println(author + " " + date);
        System.out.println(message);
    }

    abstract long maxSizeEntry();
}
